# Author: Kiran Ranganath, University of California Riverside
# CS203: Advanced Computer Architecture
# Lab-3: Cache simulator

from terminaltables import SingleTable
import click
import math


class Utils(object):

    def __init__(self, *args):
        self.hits = 0
        self.misses = 0
        self.mapping = args[0]
        self.cache_size = args[1]
        self.block_size = args[2]
        self.tracefile = args[3]
        self.sets = 0
        self.ways = 0
        self.cold = 0
        self.conflict = 0
        self.capacity = 0
        self.utilized = 0
        self.cache = {}
        self.trace = []
        self.count = 0

    def readfile(self):
        print "\nReading %s ...\n" % self.tracefile
        with open(self.tracefile) as content:
            self.trace = content.readlines()

    def results(self):
        print "Cache size              : %sB" % self.cache_size
        print "Block size              : %sB\n" % self.block_size
        print "Number of Addresses     : %d\n" % self.count
        print "Number of Cache hits    : %d" % self.hits
        print "(Hit rate = {0} %)\n".format(float(self.hits)*100.0 /
                                           float(self.count))
        print "Number of Cache misses  : %d" % self.misses
        print " | (Miss rate = {0} %)".format(float(self.misses)*100.0 /
                                             float(self.count))
        print " |-Cold misses      : %d" % self.cold
        print " |-Conflict misses  : %d" % self.conflict
        print " |-Capacity misses  : %d\n" % self.capacity
        print "Utilization factor      : {0} %".format(float(self.utilized) *
                                                      100.0 / float(self.ways *
                                                      self.sets))
        print " |-Available blocks : %d" % (self.ways * self.sets)
        print " |-Utilized blocks  : %d\n" % self.utilized

    def compute_size(self, size):
        multiplier = 1
        if size.endswith("K"):
            multiplier = 1024
            size = size[0:len(size)-1]
        elif size.endswith("M"):
            multiplier = 1024 * 1024
            size = size[0:len(size)-1]
        size = int(size) * multiplier
        return size

    def print_table(self, table_name, table_values):
        table_instance = SingleTable(table_values, table_name)
        for col in range(len(table_values[0])):
            table_instance.justify_columns[col] = "center"
        print table_instance.table

class CacheSim(Utils):

    def __init__(self, *args):
        Utils.__init__(self, *args)
        self.total_size = self.compute_size(self.cache_size)
        self.block = self.compute_size(self.block_size)
        if self.mapping == "direct":
            self.ways = 1
            self.sets = self.total_size/self.block

        elif self.mapping == "2-way-set-associative":
            self.ways = 2
            self.sets = self.total_size/self.block/2

        elif self.mapping == "4-way-set-associative":
            self.ways = 4
            self.sets = self.total_size/self.block/4

        elif self.mapping == "8-way-set-associative":
            self.ways = 8
            self.sets = self.total_size/self.block/8

        elif self.mapping == "16-way-set-associative":
            self.ways = 16
            self.sets = self.total_size/self.block/16

        else:
            self.ways = self.total_size/self.block
            self.sets = 1


    def initialize_cachesim(self):
        print "Initializing %s Cache of %s bytes ...\n" % (self.mapping,
                                                           self.cache_size)
        for cache_set in range(self.sets):
            self.cache[cache_set] = {}
            for way in range(self.ways):
                self.cache[cache_set][way] = {}
                self.cache[cache_set][way]["tag"] = 0
                self.cache[cache_set][way]["valid"] = 0
                self.cache[cache_set][way]["priority"] = self.ways-way
        print "**Initialization complete**\n"

    def cachesim(self, tag, index):
        for way in range(self.ways):
            if self.cache[index][way]["tag"] == tag:
                self.hits += 1
                if self.ways > 1:
                    for way2 in range(self.ways):
                        if (self.cache[index][way2]["priority"] <
                                self.cache[index][way]["priority"]):
                            self.cache[index][way2]["priority"] += 1
                    self.cache[index][way]["priority"] = 1
                return
        self.misses += 1
        if self.ways > 1:
            for way in range(self.ways):
                if self.cache[index][way]["priority"] == self.ways:
                    self.cache[index][way]["tag"] = tag
                    self.cache[index][way]["priority"] = 1
                    if self.cache[index][way]["valid"] == 0:
                        self.cold += 1
                    else:
                        self.conflict += 1
                    self.cache[index][way]["valid"] = 1
                    continue
                self.cache[index][way]["priority"] += 1
        else:
            self.cache[index][0]["tag"] = tag
            if self.cache[index][way]["valid"] == 0:
                self.cold += 1
            else:
                self.conflict += 1
            self.cache[index][way]["valid"] = 1

    def run(self):
        self.readfile()
        offset_bits = int(math.log(self.block, 2))
        index_bits = int(math.log(self.sets, 2))
        tag_bits = 32 - offset_bits - index_bits
        address_format = [
            ["Tag", "Index", "Offset"],
            [tag_bits, index_bits, offset_bits]
        ]
        self.print_table("Address", address_format)
        self.initialize_cachesim()
        for address in self.trace:
            address = int(address.split()[2], 16)
            address %= (2**32)
            index = address >> int(math.log(self.block, 2))
            index %= self.sets
            tag = address >> int(math.log(self.sets, 2) +
                                 math.log(self.block, 2))
            self.cachesim(tag, index)
            self.count += 1
        for way in range(self.ways):
            for index in range(self.sets):
                if not self.cache[index][way]["valid"]:
                    self.utilized += 1
        if self.mapping == "fully-associative":
            self.conflict, self.capacity = self.capacity, self.conflict


@click.command()
@click.option("--mapping", prompt="Enter Cache Mapping technique",
              type=click.Choice(["direct",
                                 "2-way-set-associative",
                                 "4-way-set-associative",
                                 "8-way-set-associative",
                                 "16-way-set-associative",
                                 "fully-associative"]))
@click.option("--cache-size", "cache_size",
              prompt="Enter Cache size (in bytes)")
@click.option("--block-size", "block_size",
              prompt="Enter Cache Block size (in bytes)")
@click.argument("tracefile", type=click.Path(exists=True))
def main(mapping, cache_size, block_size, tracefile):
    simulator = CacheSim(mapping, cache_size, block_size, tracefile)
    simulator.run()
    simulator.results()


if __name__ == "__main__":
    main()
