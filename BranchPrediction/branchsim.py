# Author: Kiran Ranganath, University of California Riverside
# CS203: Advanced Computer Architecture
# Lab-2: Branch Prediction algorithms

import sys


class Utils(object):

    def __init__(self, *args):
        self.hits = 0
        self.misses = 0
        self.filename = args[0]
        self.m = int(args[1])
        self.n = int(args[2])
        self.lsb = int(args[3])
        self.branches = 0
        self.trace = []
        self.utilized = 0

    def readfile(self):
        with open(self.filename) as content:
            self.trace = content.readlines()

    def results(self):
        hitrate = (float(self.hits) / float(self.branches)) * 100
        missrate = (float(self.misses) / float(self.branches)) * 100
        entries = 2**self.m * 2**self.lsb
        print "Hits:", self.hits
        print "Misses:", self.misses
        print "Branches:", self.branches
        print "Hit rate:", hitrate, "%"
        print "Miss rate:", missrate, "%"
        print "Utilized %d entries out of available %d entries" % (
            self.utilized, entries)


class Predictor(Utils):

    def __init__(self, *args):
        Utils.__init__(self, *args)
        self.BTB = {}
        self.gbh = 0

    def initialize_tables(self):
        for global_index in range(2**self.m):
            self.BTB[global_index] = {}
            for index in range(2**self.lsb):
                self.BTB[global_index][index] = {}
                self.BTB[global_index][index]["prediction"] = 0
                self.BTB[global_index][index]["utilized"] = 0

    def predictor(self, index, action):
        self.branches += 1
        self.BTB[self.gbh][index]["utilized"] = 1
        if self.n == 1:
            if (action == "T"):
                if self.BTB[self.gbh][index]["prediction"]:
                    self.hits += 1
                else:
                    self.BTB[self.gbh][index]["prediction"] = 1
                    self.misses += 1
                self.gbh = (((self.gbh << 1) + 1) % (2 ** self.m))
            else:
                if not self.BTB[self.gbh][index]["prediction"]:
                    self.hits += 1
                else:
                    self.BTB[self.gbh][index]["prediction"] = 0
                    self.misses += 1
                self.gbh = (((self.gbh << 1) + 0) % (2 ** self.m))
        else:
            if (action == "T"):
                if not self.BTB[self.gbh][index]["prediction"]:
                    self.BTB[self.gbh][index]["prediction"] += 1
                    self.misses += 1
                elif (self.BTB[self.gbh][index]["prediction"] == 1):
                    self.BTB[self.gbh][index]["prediction"] += 2
                    self.misses += 1
                elif (self.BTB[self.gbh][index]["prediction"] == 2):
                    self.BTB[self.gbh][index]["prediction"] += 1
                    self.hits += 1
                else:
                    self.hits += 1
                self.gbh = (((self.gbh << 1) + 1) % (2 ** self.m))
            else:
                if not self.BTB[self.gbh][index]["prediction"]:
                    self.hits += 1
                elif (self.BTB[self.gbh][index]["prediction"] == 1):
                    self.BTB[self.gbh][index]["prediction"] -= 1
                    self.hits += 1
                elif (self.BTB[self.gbh][index]["prediction"] == 2):
                    self.BTB[self.gbh][index]["prediction"] -= 2
                    self.misses += 1
                else:
                    self.BTB[self.gbh][index]["prediction"] -= 1
                    self.misses += 1
                self.gbh = (((self.gbh << 1) + 0) % (2 ** self.m))

    def run(self):
        print "Loading ", self.filename
        self.readfile()
        self.initialize_tables()
        for branch in self.trace:
            address, action = branch.split()
            index = int(address, 16) % (2**int(self.lsb))
            self.predictor(index, action)
        for global_index in range(2**self.m):
            for index in range(2**self.lsb):
                if self.BTB[global_index][index]["utilized"]:
                    self.utilized += 1


def main(*args):
    try:
        if int(args[2].split(",")[0]) not in range(17):
            print "The value of m should be between 0 and 16"
            sys.exit()
        if int(args[2].split(",")[1]) not in [1, 2]:
            print "The value of n should either be 1 or 2"
            sys.exit()
        if int(args[3]) not in range(0, 17):
            print "The value of LSB should be between 1 and 16"
            sys.exit()
    except IndexError:
        print "Usage: python branchsim.py filename m,n LSB"
        sys.exit()
    prediction = Predictor(args[1], args[2].split(",")[0],
                           args[2].split(",")[1], args[3])
    prediction.run()
    prediction.results()

if __name__ == "__main__":
    main(*sys.argv)
